var ruta_universal = baseurl

function listarComboCity(){
	var cbx = '';
	$.ajax({
		url: ruta_universal + 'Filter/get_city_list',
		type:'POST',
		cache:false,
		success:function(data){			
			var datos = JSON.parse(data)
			$.each(datos, function(i, item){		
				cbx += '<option value="' + item.id + '">' + item.valor +'</option>';			
			})
			$("#cbxListCitys").append(cbx)
		}
	})
}
listarComboCity();

function listarTipoId(){
	var cbx = '';
	$.ajax({
		url: ruta_universal + 'Filter/get_type_id_list',
		type:'POST',
		cache:false,
		success:function(data){			
			var datos = JSON.parse(data)
			$.each(datos, function(i, item){		
				cbx += '<option value="' + item.id + '">' + item.valor +'</option>';			
			})
			$("#cbxListTipe").append(cbx)
		}
	})
}
listarTipoId();

function listarProyect(){
	var cbx = '';
	$.ajax({
		url: ruta_universal + 'Filter/get_proyect_list',
		type:'POST',
		cache:false,
		success:function(data){			
			var datos = JSON.parse(data)
			$.each(datos, function(i, item){		
				cbx += '<option value="' + item.id + '">' + item.name +'</option>';			
			})
			$("#cbxListproyect").append(cbx)
		}
	})
}
listarProyect();

function listarProfile(){
	var cbx = '';
	$.ajax({
		url: ruta_universal + 'Filter/get_profile_list',
		type:'POST',
		cache:false,
		success:function(data){			
			var datos = JSON.parse(data)
			$.each(datos, function(i, item){		
				cbx += '<option value="' + item.id + '">' + item.valor +'</option>';			
			})
			$("#cbxListPerfil").append(cbx)
		}
	})
}
listarProfile();

function listarUser(){
	var cbx = '';
	$.ajax({
		url: ruta_universal + 'Filter/get_user_list',
		type:'POST',
		cache:false,
		success:function(data){			
			var datos = JSON.parse(data)
			$.each(datos, function(i, item){		
				cbx += '<option value="' + item.id + '">' + item.username +'</option>';			
			})
			$("#cbxListUser").append(cbx)
		}
	})
}
listarUser();

function listarMenu(){
	var cbx = '';
	$.ajax({
		url: ruta_universal + 'Filter/get_menu_list',
		type:'POST',
		cache:false,
		success:function(data){			
			var datos = JSON.parse(data)
			$.each(datos, function(i, item){		
				cbx += '<option value="' + item.id + '">' + item.name +'</option>';			
			})
			$("#cbxListMenu").append(cbx)
		}
	})
}
listarMenu();