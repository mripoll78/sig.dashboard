var rutaUniversal = baseurl;

function ListarProyect(){
	var cont = 0
	$.ajax({
		url: rutaUniversal + 'Proyect/proyect_list',
		type:'POST',
		cache:false,
		success:function(data){
            var tabla = JSON.parse(data);
			$.each(tabla, function(i, item){
				cont++
				$("#tbodyProyect").append(
					'<tr>'+
						'<td>' + cont + '</td>' +
						'<td>' + item.name +'</td>'+
						'<td>' + item.manager +'</td>'+
						'<td>' + item.phone +'</td>' +
						'<td>' + item.email +'</td>' +
						'<td>' + item.address +'</td>' +
						'<td>' + item.id_city +'</td>' +
						'<td>' + item.date_system +'</td>' +
						'<td>' +	
							'<a onclick="cargarPagina(\'proyect\',\'edit_proyect\')" class="btn btn-warning btn-circle btn-sm">' + 
								'<i class="fas fa-edit"></i>' +
							'</a>' +
							'<a href="#" class="btn btn-danger btn-circle btn-sm">' +
								'<i class="fas fa-trash"></i>' +
							'</a>' +
						'</td>' + 
					'</tr>'
				)
			})
		}
	})
}

ListarProyect()