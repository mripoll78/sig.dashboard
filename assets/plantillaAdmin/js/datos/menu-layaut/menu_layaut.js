var rutaUniversal = baseurl;

function ListarMenu(){
	var menu = '';
    $.ajax({
        url: rutaUniversal + 'Header/get_menu',
        type:'GET',
        cache:false,
        success:function(data){
            var items = JSON.parse(data)
			$.each(items.arrayMenus, function(i, item){
                menu += '<li class="nav-item">';
                    menu += '<a class="nav-link" href="#" data-toggle="collapse" data-target="#'+ item.collapse +'" aria-expanded="true" aria-controls="'+ item.collapse +'">';
                        menu += '<i class="'+ item.icono + '"></i>';
                        menu += '<span >'+ item.name +'</span>'
                    menu += '</a>'
                    var submenusH2 = $.grep(items.arrayMenusH2, function (e) { return e.id_menu_father == item.id; });
                    menu += '<div id="'+ item.collapse +'" class="collapse" aria-labelledby="'+ item.heading +'" data-parent="#accordionSidebar">';
                        menu += '<div class="bg-white py-2 collapse-inner rounded">';
                        $.each(submenusH2, function(i, item2){
                            // menu += '<a class="collapse-item" onclick="cargarPagina(\'' + item2.binder + '\',\'' + item2.view + '\')">'+ item2.name +'</a>';
                            menu += '<a class="collapse-item" href="'+ rutaUniversal + item2.binder +'/'+ item2.view + '">'+ item2.name +'</a>'
                        })
                        menu += '</div>';
				    menu += '</div>';
				menu += '</li>';                
			})
			$("#menu").append(menu)
        }
    });
}
ListarMenu();

function cargarPagina(carpeta, ruta) {
    $.ajax({
		url: rutaUniversal + 'Home/CargarPagina/' + carpeta + '/' + ruta,
		type: 'POST',
		success: function(data) {
			$("#contenido").html(data);
		}
	})
}
