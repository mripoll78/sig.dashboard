var rutaUniversal = baseurl;

function ListarProyect(){
	var cont = 0
	$.ajax({
		url: rutaUniversal + 'Menu/list_menu',
		type:'POST',
		cache:false,
		success:function(data){
            var tabla = JSON.parse(data);
			$.each(tabla, function(i, item){
				cont++
				$("#tbodyMenu").append(
					'<tr>'+
						'<td>' + cont + '</td>' +
						'<td>' + item.name_menu +'</td>'+
						'<td>' + item.username +'</td>'+
						'<td>' + item.name +'</td>' +
						'<td>' + item.date_system +'</td>' +
						'<td>' +	
							'<a href="#" class="btn btn-warning btn-circle btn-sm">' + 
								'<i class="fas fa-edit"></i>' +
							'</a>' +
							'<a href="#" class="btn btn-danger btn-circle btn-sm">' +
								'<i class="fas fa-trash"></i>' +
							'</a>' +
						'</td>' + 
					'</tr>'
				)
			})
		}
	})
}

ListarProyect()