var rutaUniversal = baseurl;

function Listarusuario(){
	var cont = 0
	$.ajax({
		url: rutaUniversal + 'Empleado/user_list_empleado',
		type:'POST',
		cache:false,
		success:function(data){
            var tabla = JSON.parse(data);
			$.each(tabla, function(i, item){
				cont++
				$("#tbodyUsuario").append(
					'<tr>'+
						'<td>' + cont + '</td>' +
						'<td>' + item.one_name +'</td>'+
						'<td>' + item.one_last_name +'</td>'+
						'<td>' + item.phone +'</td>' +
						'<td>' + item.email +'</td>' +
						'<td>' + item.date_system +'</td>' +
						'<td>' +	
							'<a onclick="cargarPagina(\'empleado\',\'edit_empleado\')" class="btn btn-warning btn-circle btn-sm">' + 
								'<i class="fas fa-edit"></i>' +
							'</a>' +
							'<a href="#" class="btn btn-danger btn-circle btn-sm">' +
								'<i class="fas fa-trash"></i>' +
							'</a>' +
						'</td>' + 
					'</tr>'
				)
			})
		}
	})
}

Listarusuario()