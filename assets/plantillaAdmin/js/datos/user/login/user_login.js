var rutaUniversal = baseurl;

function Registrar(){  
    var txtUsername  = $("#txtUsername").val();
    var txtPass  = $("#txtPass").val();   
    
    if(txtUsername === '' && txtPass === ''){
        $("#txtUsername").focus();  
    }else{
        if(txtUsername === ''){
            $("#txtUsername").focus();            
        }else{
            if(txtPass === ''){
                $("#txtPass").focus();
            }else{
                $.ajax({
                    url: rutaUniversal + 'Login/login_user', 
                    type: 'POST',
                    data: {'txtUsername'  : txtUsername, 'txtPass' : txtPass },
                    success: function (data) {
                    console.log(data);
                    var items = JSON.parse(data); 
                    if(items.status != 'Invalid Login' && items.status != 'Invalid Usuario'){
                            localStorage.setItem("token", items.token);
                            location.href = rutaUniversal + "Home";
                        }else{
                            document.getElementById('diverror').style.visibility = 'visible';
                        }
                    }
                }).fail( function() {
                    alert("Usuario incorrecto");    
                }); 
            }
        }
    }
}