<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>House pet - Dashboard</title>

  <link href="<?php echo base_url();?>assets/plantillaAdmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
  <link href="<?php echo base_url();?>assets/plantillaAdmin/css/sb-admin-2.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/plantillaAdmin/css/select.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/select-bootstrap/dist/css/bootstrap-select.min.css">

<Style>
  .bg-gradient-primary {
    background-color: #1cc88a;
    background-image: linear-gradient(180deg,#1cc88a 10%,#1cc88a 100%);
    background-size: cover;
  }
  .btn-primary {
    color: #fff;
    background-color: #1cc88a;
    border-color: #1cc88a;
  }
</Style>
</head>