<body id="page-top" class="sidebar-toggled">
  <div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= site_url('Home'); ?>">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">House <sup>Pet</sup></div>
      </a>
      <hr class="sidebar-divider my-0">
      <li class="nav-item active">
        <a class="nav-link" style="cursor: pointer;" onclick="cargarPagina('dashboard-page','dashboard')">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Opciones
      </div>
      <div class="main-menu" id="menu">
          <!--<li class="active">
                <a href="index-2.html"><i class="zmdi zmdi-home"></i> Home</a>
            </li>-->
      </div>
      <hr class="sidebar-divider d-none d-md-block">
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
    </ul>

<script> 
  var baseurl = "<?php echo base_url(); ?>"
</script>

<script src="<?php echo base_url();?>assets/plantillaAdmin/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plantillaAdmin/js/datos/menu-layaut/menu_layaut.js"></script>

