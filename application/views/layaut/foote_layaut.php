
      <!-- <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; House pet 2020</span>
          </div>
        </div>
      </footer> -->
    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">preparado para partir?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Seleccione "Cerrar sesión" a continuación si está listo para finalizar su sesión actual.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-primary" href="<?= site_url('Login/logout'); ?>">Cerrar sessión</a>
        </div>
      </div>
    </div>
  </div>

  <script src="<?php echo base_url();?>assets/plantillaAdmin/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/plantillaAdmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>assets/plantillaAdmin/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="<?php echo base_url();?>assets/plantillaAdmin/js/sb-admin-2.min.js"></script>
  <script src="<?php echo base_url();?>assets/plantillaAdmin/vendor/chart.js/Chart.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/assets/select-bootstrap/dist/js/bootstrap-select.min.js"></script>
  
  <!-- <script src="<?php echo base_url();?>assets/plantillaAdmin/js/demo/chart-area-demo.js"></script> -->
  <!-- <script src="<?php echo base_url();?>assets/plantillaAdmin/js/demo/chart-pie-demo.js"></script> -->
</body>
</html>