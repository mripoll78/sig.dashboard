<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>House pet - Registrar</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>assets/plantillaAdmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url();?>assets/plantillaAdmin/css/sb-admin-2.min.css" rel="stylesheet">
  <Style>
    .text-xs.font-weight-bold.text-danger.text-uppercase.mb-1 {
      text-align: center;
      visibility: hidden;
    }
    .custom-control.custom-radio.custom-control-inline {
      margin-left: 4%;
    }
    @media only screen and (max-width: 700px) {
        .custom-control.custom-radio.custom-control-inline {
        margin-left: 14%;
      }
    }
  </Style>
</head>

<body class="bg-gradient-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Crea una cuenta!</h1>
              </div>
              <form class="user">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="txtNombre" placeholder="Nombre">
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" id="txtApellido" placeholder="Apellido">
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" id="txtUser" placeholder="Usuario">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control form-control-user" id="txtCorreotele" placeholder="Correo o numero de telefono">
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" class="form-control form-control-user" id="txtPass" placeholder="Contraseña">
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control form-control-user" id="txtConfiPass" placeholder="Confirmar Contraseña">
                  </div>
                </div>
                <div class="form-group row">  
                  <!-- Default inline 1-->
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" value="1" class="custom-control-input" id="rfemale" name="radiovalidate">
                    <label class="custom-control-label" for="rfemale">Hombre</label>
                  </div>
                  <!-- Default inline 2-->
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" value="2" class="custom-control-input" id="rmale" name="radiovalidate">
                    <label class="custom-control-label" for="rmale">Mujer</label>
                  </div>
                </div>
                <div id="diverror" class="text-xs font-weight-bold text-danger text-uppercase mb-1">Contraseñas no son iguales!</div>
                </br>
                <a href="#" onclick="Registrar()" class="btn btn-primary btn-user btn-block">
                  Crear Cuenta
                </a>
                <!-- <hr>
                <a href="index.html" class="btn btn-google btn-user btn-block">
                  <i class="fab fa-google fa-fw"></i> Register with Google
                </a>
                <a href="index.html" class="btn btn-facebook btn-user btn-block">
                  <i class="fab fa-facebook-f fa-fw"></i> Register with Facebook
                </a> -->
              </form>
              <!-- <hr> -->
              <div class="text-center">
                <a class="small" href="<?= site_url('user/user_forgot_password'); ?>">¿Se te olvidó tu contraseña?</a>
              </div>
              <div class="text-center">
                <a class="small" href="<?= site_url('login'); ?>">¿Ya tienes una cuenta? ¡Inicia sesión!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/plantillaAdmin/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/plantillaAdmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url();?>assets/plantillaAdmin/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url();?>assets/plantillaAdmin/js/sb-admin-2.min.js"></script>

  <script>
    var baseurl = "<?php echo base_url(); ?>"
  </script>

  <script src="<?php echo base_url(); ?>assets/plantillaAdmin/js/datos/user/user_register.js"></script>
</body>

</html>
