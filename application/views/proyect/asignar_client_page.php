<style>
a.btn.btn-warning.btn-circle.btn-sm {
    margin-right: 13%;
}
.select.user {
    margin-left: 33.5%;
    margin-right: 0%;
}
.select.profile {
    margin-left: 33.5%;
    margin-top: 2%;
}
input#txtIdentificacion {
    margin-top: 5%;
}
i.fas.fa-edit {
    color: white;
}
input#txtDireccion {
    margin-top: 5%;
}
input#cbxListPerfil {
    margin-top: 5%;
}
input#txtDireccion {
    margin-top: 5%;
}
a.btn.btn-light.btn-circle.btn-sm {
    margin-right: 13%;
}
.col-sm-6 {
    flex: 0 0 50%;
    max-width: 0%;
    margin-left: -3%;
}
.form-group.radio {
    margin-left: 40%;
    margin-top: 2%;
}
.form-group {
    margin-left: 34%;
    width: 28%;
}
a.btn.btn-primary.btn {
    margin-right: 47%;
    color: white;
}
.form-group.select {
    margin-left: 31%;
}
.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
    width: 298px;
}
@media only screen and (max-width: 700px) {
    .custom-control.custom-radio.custom-control-inline {
        margin-left: 12%;
    }
    .col-sm-6 {
        flex: 0 0 50%;
        max-width: 0%;
        margin-left: -4%;
    }
    .form-group.radio {
        margin-left: 0%;
    }
    .form-group {
        width: 100%;
        margin-left: 0%;
    }
    .input-group {
        margin-left: 5%;
    }
    a.btn.btn-primary.btn {
        margin-right: 0%;
    }
    .form-group.select {
        margin-left: -12%;
    }
    .modal-footer {
        margin-right: 22%;
    }
}
</style>
  <h1 class="h3 mb-2 text-gray-800">Crear cliente y asignar proyecto</h1>
  <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p>

<form class="user">
   <div class="row">        
    <div class="form-group">
        <input type="text" class="form-control form-control-user" id="txtNombre" name="txtNombre"  placeholder="Nombre">
    </div>
    <div class="form-group">
        <input type="text" class="form-control form-control-user" id="txtApellido" name="txtApellido" placeholder="Apellido">
    </div>
    <div class="input-group">
        <div class="select user">
            <select name="cbxListTipe" id="cbxListTipe">
                <option selected disabled>Seleccione tipo identificacion</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <input type="number" class="form-control form-control-user" id="txtIdentificacion" name="txtIdentificacion" placeholder="Identificación">
    </div>
  </div>
  <div class="row">
    <div class="form-group">
        <input type="email" class="form-control form-control-user" id="txtEmail" name="txtEmail" placeholder="Correo">
    </div>
    <div class="form-group">
        <input type="number" class="form-control form-control-user" id="txtTelefono" name="txtTelefono" placeholder="Telefono">
    </div>
    <div class="input-group">
        <div class="select user">
            <select name="cbxListCitys" id="cbxListCitys">
                <option selected disabled>Seleccione una ciudad</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <input type="text" class="form-control form-control-user" id="txtDireccion" name="txtDireccion" placeholder="Direccion">
    </div>
    <div class="input-group">
        <div class="input-group">
            <div class="select user">
                <select name="cbxListproyect" id="cbxListproyect">
                    <option selected disabled>Seleccione un proyecto</option>
                </select>
            </div>
        </div>
    </div>
    <div class="input-group">
        <div class="input-group">
            <div class="select profile">
                <select name="cbxListPerfil" id="cbxListPerfil">
                    <option selected disabled>Seleccione un perfil</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group radio">  
        <!-- Default inline 1-->
        <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" value="1" class="custom-control-input" id="rfemale" name="radiovalidate">
        <label class="custom-control-label" for="rfemale">Hombre</label>
        </div>
        <!-- Default inline 2-->
        <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" value="2" class="custom-control-input" id="rmale" name="radiovalidate">
        <label class="custom-control-label" for="rmale">Mujer</label>
        </div>          
    </div>
  </div>
    <div class="modal-footer">
        <a class="btn btn-primary btn" onclick="RegistrarCliente()">Crear Usuario</a>
    </div>
</form>
<script> 
    var baseurl = "<?php echo base_url(); ?>"   
</script>

<script src="<?php echo base_url(); ?>assets/plantillaAdmin/js/datos/filter/filter.js"></script>
<script src="<?php echo base_url(); ?>assets/plantillaAdmin/js/datos/user/admin/user_register.js"></script>

