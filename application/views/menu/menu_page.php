<style>
.btn-primary.agig {
    height: 0%;
    margin-top: 0.5%;
    color: white !important;
}
  a.btn.btn-warning.btn-circle.btn-sm {
    margin-right: 13%;
  }
  a.btn.btn-light.btn-circle.btn-sm {
    margin-right: 13%;
  }
  .col-sm-6 {
      flex: 0 0 50%;
      max-width: 0%;
      margin-left: -3%;
  }
  .form-group.row {
      margin-left: 2%;
  }
  .input-group {
    margin-left: 12%;
}
  @media (min-width: 768px){
    .ml-md-3, .mx-md-3 {
      margin-left: 69% !important;
    }
}
  @media only screen and (max-width: 700px) {
    .custom-control.custom-radio.custom-control-inline {
      margin-left: 12%;
    }
    .col-sm-6 {
        flex: 0 0 50%;
        max-width: 0%;
        margin-left: -4%;
    }
    .form-group.row {
        margin-left: -10%;
    }
    a.btn.btn-primary.btn {
        margin-right: 0% !important;
    }
}
</style>
    <div class="input-group">
        <div class="select user">
            <select name="cbxListUser" id="cbxListUser">
                <option selected disabled>Seleccione un usuario</option>
            </select>
        </div>
        <div class="select menu">
            <select name="cbxListMenu" id="cbxListMenu">
                <option selected disabled>Seleccione un menu</option>
            </select>   
        </div>
        <a class="btn btn-primary agig" onclick="RegistrarMenu()">Asignar</a>
    </div></br>
  <div class="card shadow mb-4">
  <div class="card-header py-3">
  <a class="m-0 font-weight-bold text-primary" style="cursor: pointer; text-align: right;" href="#">Asignar Menu</a>
    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
    <div class="input-group">
        <input type="text" class="form-control bg-light border-0 small" placeholder="Consulta" aria-label="Search" aria-describedby="basic-addon2">
        <div class="input-group-append">
        <button class="btn btn-primary" type="button">
            <i class="fas fa-search fa-sm"></i>
        </button>
        </div>
    </div>
    </form>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
          <th>#</th>
            <th>Nombre menu</th>
            <th>Usuario</th>
            <th>Tipo menu</th>
            <th>Fecha</th>
            <th>Acciones</th>   
          </tr>
        </thead>
        <tbody id="tbodyMenu">

        </tbody>
      </table>
    </div>
  </div>
</div>

<script> 
    var baseurl = "<?php echo base_url(); ?>"   
</script>
<script src="<?php echo base_url(); ?>assets/plantillaAdmin/js/datos/filter/filter.js"></script>
<script src="<?php echo base_url(); ?>assets/plantillaAdmin/js/datos/menu-layaut/admin/register_asig_menu.js"></script>
<script src="<?php echo base_url(); ?>assets/plantillaAdmin/js/datos/menu-layaut/admin/list_menu.js"></script>
