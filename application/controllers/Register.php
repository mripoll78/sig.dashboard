<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Register extends CI_Controller {

	function __Construct(){	
        
        parent::__Construct();
		$this->load->helper('url');
    }

	public function index()
	{
		$this->load->view('user-autenticate/user_register');
    }
    
    public function register_user()
	{
		if ($this->input->is_ajax_request()) {

			$txtNombre  = $this->input->post('txtNombre');
			$txtApellido  = $this->input->post('txtApellido');
			$txtUser  = $this->input->post('txtUser');
			$txtCorreotele  = $this->input->post('txtCorreotele');
			$txtPass  = $this->input->post('txtPass');
			$rGender  = $this->input->post('rGender');
			
			if(is_integer($txtCorreotele)){
				$parameters_user = [
					'one_name' => $txtNombre,
					'one_last_name' => $txtApellido,
					'phone' => $txtCorreotele,
					'username' => $txtUser,
					'password' => $txtPass,
					'id_profile' => 3,
					'id_gender' => $rGender
				  ];
			}else{
				$parameters_user = [
					'one_name' => $txtNombre,
					'one_last_name' => $txtApellido,
					'email' => $txtCorreotele,
					'username' => $txtUser,
					'password' => $txtPass,
					'id_profile' => 3,
					'id_gender' => $rGender
				  ];
			}

			$processed = FALSE;
			$ERROR_MESSAGE = '';

			$ch = curl_init();
			// curl_setopt($ch, CURLOPT_URL, "https://aniface.online/webservice/api/user/Register/register_user");
			curl_setopt($ch, CURLOPT_URL, "http://localhost/webserviceaniface/api/user/Register/register_user");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters_user);   // post data
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$json = curl_exec($ch);
			curl_close ($ch);

			$obj = json_decode($json);

			echo json_encode($obj);
		}
	}
}
