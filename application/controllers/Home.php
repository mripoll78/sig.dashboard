<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Home extends CI_Controller {

    function __Construct(){
        parent::__Construct();
        $this->load->library('session');
        $this->load->helper('url');
    }

    public function index()
    {
        $datos['one_name'] = $this->session->userdata('one_name');
        $datos['one_last_name'] = $this->session->userdata('one_last_name');
        
        $this->load->view('layaut/head_layaut');
        $this->load->view('layaut/header_layaut');
        $this->load->view('layaut/nav_layaut', $datos);
        $this->load->view('layaut/body_layaut'); 
        $this->load->view('layaut/foote_layaut');
    }

    public function CargarPagina($carpeta, $ruta){
		$this->load->view($carpeta . '/' . $ruta);
	}
}
