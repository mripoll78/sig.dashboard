<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Empleado extends CI_Controller {

	function __Construct(){	
        
        parent::__Construct();
		$this->load->library('session');
		$this->load->model('M_register');
		$this->load->Model('M_user');
		$this->load->helper('url');
    }
    
    public function index()
    {
        $datos['one_name'] = $this->session->userdata('one_name');
        $datos['one_last_name'] = $this->session->userdata('one_last_name');
        
        $this->load->view('layaut/head_layaut');
        $this->load->view('layaut/header_layaut');
		$this->load->view('layaut/nav_layaut', $datos);		
		$this->load->view('layaut/body_layaut'); 
        $this->load->view('user-page/user_empleado_page'); 		
        $this->load->view('layaut/foote_layaut');
	}
	
	public function create_empleado()
    {
        $datos['one_name'] = $this->session->userdata('one_name');
        $datos['one_last_name'] = $this->session->userdata('one_last_name');
        
        $this->load->view('layaut/head_layaut');
        $this->load->view('layaut/header_layaut');
		$this->load->view('layaut/nav_layaut', $datos);		
		$this->load->view('layaut/body_layaut'); 
        $this->load->view('user-page/create_empleado_page'); 		
        $this->load->view('layaut/foote_layaut');
    }

	function user_list_empleado()
    {
		$id_draft  = $this->session->userdata('id_proyect');
		$data = $this->M_user->lister_user_empleado($id_draft);
        echo json_encode($data);  	
	}

	public function register_empleado()
	{	
		if ($this->input->is_ajax_request()) {

			$id_gender = $this->input->post('rGender');
			$id_city = $this->input->post('CbxListCity');
			$id_type = $this->input->post('CbxTipoId');
			$identification = $this->input->post('txtIdentificacion');
			$one_name = $this->input->post('txtNombre');
			$one_last_name = $this->input->post('txtApellido');
			$email = $this->input->post('txtEmail');
			$phone = $this->input->post('txtTelefono');
			$address = $this->input->post('txtDireccion');
				
			// table user
			$id_profile = 5;
			$username = $this->input->post('txtIdentificacion');
			$password = $this->input->post('txtIdentificacion');

			// table proyecto 
			$id_draft = $this->session->userdata('id_proyect');


			$data1 = $this->M_register->get_verifi_people_id($identification); // validamos que la identificacion no exista
			$data2 = $this->M_register->get_verifi_user_username($username); // validamos que el usuario no exista
			$data3 = $this->M_register->get_verifi_people_email($email); // validamos que el correo no exista
			
			if(!$data1){ 
				if(!$data2){ 
					if(!$data3){ 
						if(!$data1 && !$data2 && !$data3){   

							//creamos el array de la persona
							$datos_people =  array('id_status' => 1,
													'id_gender' => $id_gender,
													'id_city' => $id_city,
													'id_type' => $id_type,
													'identification' => $identification,
													'one_name' => $one_name,
													'one_last_name' => $one_last_name,
													'email' => $email,
													'phone' => $phone,
													'address' => $address);            
							$people_id = $this->M_register->post_people($datos_people);  // insertamos la persona
				
							// creamos el array de el usuario
							$datos_user =  array('id_profile' => $id_profile,
												'id_status' => 1,
												'id_people' => $people_id,
												'username' => $username,
												'password' => sha1($password));
							$UsrId = $this->M_register->post_user($datos_user);  // insertamos el usuario

							// creamos el array de el usario proyecto
							$datos_client_proyect =  array('id_user' => $UsrId,
															'id_status' => 1,
															'id_draft' => $id_draft);
											
							$Client_id = $this->M_register->post_user_proyect($datos_client_proyect);  // insertamos el cliente proyect

							if($UsrId){
								$datos =  array('id' => 0,
												'status' => 'success',
												'messaje' => 'Empleado creado');
												echo json_encode($datos);                
							}else{
								$datos =  array('id' => 1,
												'status' => 'error',
												'messaje' => 'Empleado no creado');
												echo json_encode($datos);                
							}  
						}else{ 
							$datos =  array('id' => 1,
												'status' => 'error',
												'messaje' => 'Empleado no registrado');
												echo json_encode($datos);                
						}  
					}else{ 
						$datos =  array('id' => 1,
							'status' => 'error',
							'messaje' => 'Correo ya existe');
							echo json_encode($datos);                
					}
				}else{ 
					$datos =  array('id' => 1,
						'status' => 'error',
						'messaje' => 'Empleado ya existe');
						echo json_encode($datos);                
				}
			}else{
				$datos =  array('id' => 1,
					'status' => 'error',
					'messaje' => 'Identificación ya existe'); 
					echo json_encode($datos);                
			}
		}
	}
}