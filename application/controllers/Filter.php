<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Filter extends CI_Controller{
	
    function __Construct()
    {
       parent::__Construct();
	   $this->load->library('session');
	   $this->load->Model('M_filter');
       $this->load->helper('url');
    }

	public function get_city_list()
	{	
		$data = $this->M_filter->list_city();
        echo json_encode($data);        
	}

	public function get_type_id_list()
	{	
		$data = $this->M_filter->list_type_id();
        echo json_encode($data);   
	}

	public function get_proyect_list()
	{	
		$data = $this->M_filter->list_proyect();
        echo json_encode($data);
	}

	public function get_profile_list()
	{	
		$data = $this->M_filter->list_profile();
        echo json_encode($data);  	
	}

	public function get_user_list()
	{	
		$data = $this->M_filter->list_user();
        echo json_encode($data);
	}

	public function get_menu_list()
	{		
		$data = $this->M_filter->list_menu();
        echo json_encode($data); 
	}

}