<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Menu extends CI_Controller {

	function __Construct(){	        
		parent::__Construct();
		$this->load->library('session');
		$this->load->Model('M_menu');
		$this->load->model('M_register');
		$this->load->helper('url');
    }

	public function index()
	{
		$datos['one_name'] = $this->session->userdata('one_name');
        $datos['one_last_name'] = $this->session->userdata('one_last_name');
        
        $this->load->view('layaut/head_layaut');
        $this->load->view('layaut/header_layaut');
        $this->load->view('layaut/nav_layaut', $datos);
        $this->load->view('layaut/body_layaut'); 
        $this->load->view('menu/menu_page'); 
        $this->load->view('layaut/foote_layaut');
    }
    
    function list_menu()
    {	
		$data = $this->M_menu->lister_menu_admin();
        echo json_encode($data);   
	}

	public function register_asig_menu()
	{	
		if ($this->input->is_ajax_request()) {
			$id_menu = $this->input->post('cbxListMenu'); 
			$id_usuario = $this->input->post('cbxListUser');   

			//creamos el array del Proyect
			$datos_asig =  array('id_menu' => $id_menu,
								'id_usuario' => $id_usuario,
								'id_status' => 1);  

			$data1 = $this->M_register->get_verifi_user_menu_id($id_usuario, $id_menu); // validamos que el usuario no tenga asignado el menu ya
			if(!$data1){
				$asig_id = $this->M_register->post_asig_menu($datos_asig);  // insertamos el la asignacion menu
				if($asig_id){
					$datos =  array('id' => 0,
									'status' => 'success',
									'messaje' => 'Asignacion creada');
					echo json_encode($datos);               
				}else{
					$datos =  array('id' => 1,
									'status' => 'error',
									'messaje' => 'Asignacion no creada');
					echo json_encode($datos);
				} 
			}else{
				$datos =  array('id' => 1,
								'status' => 'error',
								'messaje' => 'Usuario asignado ya');
				echo json_encode($datos);
			}
		}
	}
}
