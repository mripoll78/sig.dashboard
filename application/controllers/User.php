<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class User extends CI_Controller {

	function __Construct(){	        
		parent::__Construct();
		$this->load->library('session');
		$this->load->helper('url');
    }

	public function index()
	{
		$this->load->view('user-login/user_login');
	}

	function user_forgot_password()
	{
		$this->load->view('user-autenticate/user_forgot_password');
	}
}
