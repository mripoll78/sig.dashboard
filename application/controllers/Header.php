<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Header extends CI_Controller{
	
    function __Construct()
    {
       parent::__Construct();
	   $this->load->library('session');
		$this->load->Model('M_header');	   
       $this->load->helper('url');
    }

    public function get_menu()
	{	
		$id = $this->session->userdata('id');
        $data = $this->M_header->get_lister_menus_father($id);
        $data2 = $this->M_header->get_lister_menu_son($id);
        $resultado = array('arrayMenus' => $data, 'arrayMenusH2' => $data2);
        echo json_encode( $resultado);
	}
  
}
