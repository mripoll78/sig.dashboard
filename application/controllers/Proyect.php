<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Proyect extends CI_Controller {

	function __Construct(){	
        
        parent::__Construct();
		$this->load->library('session');
		$this->load->model('M_proyect');
		$this->load->model('M_register');
		$this->load->helper('url');
	}
	
	public function index()
    {
        $datos['one_name'] = $this->session->userdata('one_name');
        $datos['one_last_name'] = $this->session->userdata('one_last_name');
        
        $this->load->view('layaut/head_layaut');
        $this->load->view('layaut/header_layaut');
		$this->load->view('layaut/nav_layaut', $datos);		
		$this->load->view('layaut/body_layaut'); 
        $this->load->view('proyect/proyect_page'); 		
        $this->load->view('layaut/foote_layaut');
	}
	
	public function create_proyect()
    {
        $datos['one_name'] = $this->session->userdata('one_name');
        $datos['one_last_name'] = $this->session->userdata('one_last_name');
        
        $this->load->view('layaut/head_layaut');
        $this->load->view('layaut/header_layaut');
		$this->load->view('layaut/nav_layaut', $datos);		
		$this->load->view('layaut/body_layaut'); 
        $this->load->view('proyect/create_proyect_page'); 		
        $this->load->view('layaut/foote_layaut');
    }

    public function asignar_client()
    {
        $datos['one_name'] = $this->session->userdata('one_name');
        $datos['one_last_name'] = $this->session->userdata('one_last_name');
        
        $this->load->view('layaut/head_layaut');
        $this->load->view('layaut/header_layaut');
		$this->load->view('layaut/nav_layaut', $datos);		
		$this->load->view('layaut/body_layaut'); 
        $this->load->view('proyect/asignar_client_page'); 		
        $this->load->view('layaut/foote_layaut');
	}
	
	public function cliente_pryect()
    {
        $datos['one_name'] = $this->session->userdata('one_name');
        $datos['one_last_name'] = $this->session->userdata('one_last_name');
        
        $this->load->view('layaut/head_layaut');
        $this->load->view('layaut/header_layaut');
		$this->load->view('layaut/nav_layaut', $datos);		
		$this->load->view('layaut/body_layaut'); 
        $this->load->view('proyect/client_page'); 		
        $this->load->view('layaut/foote_layaut');
    }

	function proyect_list()
    {
		$data = $this->M_proyect->lister_proyect();
        echo json_encode($data); 	
	}

	public function register_proyect()
	{	
		if ($this->input->is_ajax_request()) {

			$name = $this->input->post('txtNombre');
			$manager = $this->input->post('txtManager'); 
			$email = $this->input->post('txtEmail');
			$id_city = $this->input->post('CbxListCity');
			$address = $this->input->post('txtDireccion');
			$phone = $this->input->post('txtTelefono'); 

			//creamos el array del Proyect
			$datos_proyect =  array('id_status' => 1,
									'id_city' => $id_city,
									'name' => $name,
									'manager' => $manager,
									'email' => $email,
									'phone' => $phone,
									'address' => $address);  

			$proyect_id = $this->M_register->post_proyect($datos_proyect);  // insertamos el Proyect
			if($proyect_id){
				$datos =  array('id' => 0,
								'status' => 'success',
								'messaje' => 'Proyecto creado');
				echo json_encode($datos);                
			}else{
				$datos =  array('id' => 1,
								'status' => 'error',
								'messaje' => 'Proyecto no creado');
				echo json_encode($datos);
			} 
		}
	}
}