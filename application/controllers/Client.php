<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Client extends CI_Controller {

	function __Construct(){	
        
        parent::__Construct();
		$this->load->library('session');
		$this->load->Model('M_user');
		$this->load->model('M_register');
		$this->load->helper('url');
	}
	
	public function index()
    {
        $datos['one_name'] = $this->session->userdata('one_name');
        $datos['one_last_name'] = $this->session->userdata('one_last_name');
        
        $this->load->view('layaut/head_layaut');
        $this->load->view('layaut/header_layaut');
		$this->load->view('layaut/nav_layaut', $datos);		
		$this->load->view('layaut/body_layaut'); 
        $this->load->view('user-page/user_cliente_page'); 		
        $this->load->view('layaut/foote_layaut');
	}
	
	public function create_client()
    {
        $datos['one_name'] = $this->session->userdata('one_name');
        $datos['one_last_name'] = $this->session->userdata('one_last_name');
        
        $this->load->view('layaut/head_layaut');
        $this->load->view('layaut/header_layaut');
		$this->load->view('layaut/nav_layaut', $datos);		
		$this->load->view('layaut/body_layaut'); 
        $this->load->view('user-page/create_client_page'); 		
        $this->load->view('layaut/foote_layaut');
    }

	function user_list_client()
    {
		$id_draft  = $this->session->userdata('id_proyect');  
		$data = $this->M_user->lister_user_client($id_draft);
        echo json_encode($data);       
	}

	public function register_client()
	{	
		if ($this->input->is_ajax_request()) {

			$one_name = $this->input->post('txtNombre');
			$one_last_name = $this->input->post('txtApellido');
			$email = $this->input->post('txtEmail');
			$identification = $this->input->post('txtIdentificacion');
			$id_type = $this->input->post('CbxTipoId');
			$id_city = $this->input->post('CbxListCity');
			$address = $this->input->post('txtDireccion');
			$phone = $this->input->post('txtTelefono');
			$id_gender = $this->input->post('rGender');
			$id_draft = $this->session->userdata('id_proyect');

			$data1 = $this->M_register->get_verifi_client_id($identification); // validamos que la identificacion no exista
			$data2 = $this->M_register->get_verifi_client_email($email); // validamos que el correo no exista
			
			if(!$data1){ 
				if(!$data2){ 
					if(!$data1 && !$data2){   

						//creamos el array del cliente
						$datos_client =  array('id_status' => 1,
												'id_gender' => $id_gender,
												'id_city' => $id_city,
												'id_type' => $id_type,
												'identification' => $identification,
												'one_name' => $one_name,
												'one_last_name' => $one_last_name,
												'email' => $email,
												'phone' => $phone,
												'address' => $address);  

						$client_id = $this->M_register->post_client($datos_client);  // insertamos el cliente
			
						// creamos el array de cliente proyecto
						$datos_client_proyect =  array('id_client' => $client_id,
														'id_status' => 1,
														'id_draft' => $id_draft);
											
						$Client_id = $this->M_register->post_client_proyect($datos_client_proyect);  // insertamos el cliente proyect
						if($Client_id){
							$datos =  array('id' => 0,
											'status' => 'success',
											'messaje' => 'Cliente creado');
							echo json_encode($datos);              
						}else{
							$datos =  array('id' => 1,
											'status' => 'error',
											'messaje' => 'Cliente no creado');
							echo json_encode($datos);
						}  
					}else{ 
						$datos =  array('id' => 1,
											'status' => 'error',
											'messaje' => 'Cliente no registrado');
						echo json_encode($datos);
					} 
				}else{ 
					$datos =  array('id' => 1,
						'status' => 'error',
						'messaje' => 'Correo ya existe');
					echo json_encode($datos);
				}
			}else{
				$datos =  array('id' => 1,
					'status' => 'error',
					'messaje' => 'Identificacion ya existe'); 
				echo json_encode($datos);
			}
		}
	}
}