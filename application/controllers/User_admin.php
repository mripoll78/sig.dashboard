<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class User_admin extends CI_Controller {

	function __Construct(){	
        
        parent::__Construct();
		$this->load->library('session');
		$this->load->model('M_user');
		$this->load->model('M_register');
		$this->load->helper('url');
    }
   
    public function register_user()
	{
		if ($this->input->is_ajax_request()) {

            $txtNombre  = $this->input->post('txtNombre');
			$txtApellido  = $this->input->post('txtApellido');
			$txtEmail  = $this->input->post('txtEmail');
			$txtIdentificacion  = $this->input->post('txtIdentificacion');
			$txtTelefono  = $this->input->post('txtTelefono');
			$txtDireccion  = $this->input->post('txtDireccion');
			$CbxTipoId  = $this->input->post('CbxTipoId');
			$CbxListCity  = $this->input->post('CbxListCity');
			$rGender  = $this->input->post('rGender');
            $cbxProyect  = $this->input->post('cbxProyect');
            $cbxProfile  = $this->input->post('cbxProfile');

			if($cbxProfile == 4){
					$one_name = $this->input->post('txtNombre');
					$one_last_name = $this->input->post('txtApellido');
					$email = $this->input->post('txtEmail');
					$identification = $this->input->post('txtIdentificacion');
					$id_type = $this->input->post('CbxTipoId');
					$id_city = $this->input->post('CbxListCity');
					$address = $this->input->post('txtDireccion');
					$phone = $this->input->post('txtTelefono');
					$id_gender = $this->input->post('rGender');
					$id_draft = $this->input->post('cbxProyect');

					$data1 = $this->M_register->get_verifi_client_id($identification); // validamos que la identificacion no exista
					$data2 = $this->M_register->get_verifi_client_email($email); // validamos que el correo no exista
					
					if(!$data1){ 
						if(!$data2){ 
							if(!$data1 && !$data2){
								//creamos el array del cliente
								$datos_client =  array('id_status' => 1,
														'id_gender' => $id_gender,
														'id_city' => $id_city,
														'id_type' => $id_type,
														'identification' => $identification,
														'one_name' => $one_name,
														'one_last_name' => $one_last_name,
														'email' => $email,
														'phone' => $phone,
														'address' => $address);  

								$client_id = $this->M_register->post_client($datos_client);  // insertamos el cliente
					
								// creamos el array de cliente proyecto
								$datos_client_proyect =  array('id_client' => $client_id,
																'id_status' => 1,
																'id_draft' => $id_draft);
													
								$Client_id = $this->M_register->post_client_proyect($datos_client_proyect);  // insertamos el cliente proyect
								if($Client_id){
									$datos =  array('id' => 0,
													'status' => 'success',
													'messaje' => 'Cliente creado');
									echo json_encode($datos);              
								}else{
									$datos =  array('id' => 1,
													'status' => 'error',
													'messaje' => 'Cliente no creado');
									echo json_encode($datos);
								}  
							}else{ 
								$datos =  array('id' => 1,
													'status' => 'error',
													'messaje' => 'Cliente no registrado');
								echo json_encode($datos);
							} 
						}else{ 
							$datos =  array('id' => 1,
								'status' => 'error',
								'messaje' => 'Correo ya existe');
							echo json_encode($datos);
						}
					}else{
						$datos =  array('id' => 1,
							'status' => 'error',
							'messaje' => 'Identificacion ya existe'); 
						echo json_encode($datos);
					}
			}
			else if($cbxProfile == 5 || $cbxProfile == 2){

				$id_gender = $this->input->post('rGender');
				$id_city = $this->input->post('CbxListCity');
				$id_type = $this->input->post('CbxTipoId');
				$identification = $this->input->post('txtIdentificacion');
				$one_name = $this->input->post('txtNombre');
				$one_last_name = $this->input->post('txtApellido');
				$email = $this->input->post('txtEmail');
				$phone = $this->input->post('txtTelefono');
				$address = $this->input->post('txtDireccion');
					
				// table user
				$id_profile = $this->input->post('cbxProfile');
				$username = $this->input->post('txtIdentificacion');
				$password = $this->input->post('txtIdentificacion');

				// table proyecto 
				$id_draft = $this->input->post('cbxProyect');

				$data1 = $this->M_register->get_verifi_people_id($identification); // validamos que la identificacion no exista
				$data2 = $this->M_register->get_verifi_user_username($username); // validamos que el usuario no exista
				$data3 = $this->M_register->get_verifi_people_email($email); // validamos que el correo no exista
				
				if(!$data1){ 
					if(!$data2){ 
						if(!$data3){ 
							if(!$data1 && !$data2 && !$data3){   

								//creamos el array de la persona
								$datos_people =  array('id_status' => 1,
														'id_gender' => $id_gender,
														'id_city' => $id_city,
														'id_type' => $id_type,
														'identification' => $identification,
														'one_name' => $one_name,
														'one_last_name' => $one_last_name,
														'email' => $email,
														'phone' => $phone,
														'address' => $address);            
								$people_id = $this->M_register->post_people($datos_people);  // insertamos la persona
					
								// creamos el array de el usuario
								$datos_user =  array('id_profile' => $id_profile,
													'id_status' => 1,
													'id_people' => $people_id,
													'username' => $username,
													'password' => sha1($password));
								$UsrId = $this->M_register->post_user($datos_user);  // insertamos el usuario

								// creamos el array de el usario proyecto
								$datos_client_proyect =  array('id_user' => $UsrId,
																'id_status' => 1,
																'id_draft' => $id_draft);
												
								$Client_id = $this->M_register->post_user_proyect($datos_client_proyect);  // insertamos el cliente proyect

								if($UsrId){
									$datos =  array('id' => 0,
													'status' => 'success',
													'messaje' => 'Usuario creado');
									echo json_encode($datos);                
								}else{
									$datos =  array('id' => 1,
													'status' => 'error',
													'messaje' => 'Usuario no creado');
									echo json_encode($datos);                
								}  
							}else{ 
								$datos =  array('id' => 1,
												'status' => 'error',
												'messaje' => 'Usuario no registrado');
								echo json_encode($datos);                
							}  
						}else{ 
							$datos =  array('id' => 1,
											'status' => 'error',
											'messaje' => 'Correo ya existe');
							echo json_encode($datos);                
						}
					}else{ 
						$datos =  array('id' => 1,
										'status' => 'error',
										'messaje' => 'Usuario ya existe');
						echo json_encode($datos);                
					}
				}else{
					$datos =  array('id' => 1,
									'status' => 'error',
									'messaje' => 'Identificación ya existe'); 
					echo json_encode($datos);                
				}
			}
		}
	}
    
    function user_list_client()
    {	
		$data = $this->M_user->lister_user_client_admin();
        echo json_encode($data); 
	}
}
