<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Login extends CI_Controller {

	function __Construct(){	
        
		parent::__Construct();
		$this->load->library('session');
		$this->load->model('M_login');
		$this->load->model('M_proyect');
		$this->load->helper('url');
    }

	public function index()
	{
		$this->load->view('user-autenticate/user_login');
    }
    
    public function login_user()
	{
		if ($this->input->is_ajax_request()) {
			
				$u = $this->input->post('txtUsername');
				$p = sha1($this->input->post('txtPass')); 
				$q = array('username' => $u); 
				$kunci = $this->config->item('thekey');
				$invalidLogin = ['status' => 'Invalid Login']; 
				$invalidUserProyect = ['status' => 'Invalid User Proyect']; 
				$invalidUser = ['status' => 'Invalid Usuario'];
				$_val_user = $this->M_login->get_user($q)->row(); 
				if($_val_user){
					$_val_dato_user = array('id_user' => $_val_user->id); 
					$_val_proyect = $this->M_proyect->get_proyect($_val_dato_user)->row();
					if($_val_proyect){
						$_val_dato_people = array('id' => $_val_user->id_people); 
						$_val_people = $this->M_login->get_people($_val_dato_people)->row(); 
						if($this->M_login->get_user($q)->num_rows() != 0){ 
							$match = $_val_user->password;   
							if($p == $match){ 
								$arraydata = array(
									'id'  => $_val_user->id,
									'username'  => $u,
									'id_profile'  => $_val_user->id_profile,
									'one_name'  => $_val_people->one_name,
									'one_last_name'  => $_val_people->one_last_name,
									'id_proyect'  => $_val_proyect->id_draft,
									'logged_in' => true,
								);
								$this->session->set_userdata($arraydata);
								echo json_encode($p);
							}else {
								echo json_encode($invalidLogin);
							}
						}else{
							echo json_encode($invalidLogin);              
						}
				}else{
					echo json_encode($invalidUserProyect);
				}     
			}else{
				echo json_encode($invalidUser);
			}
		}		
	}

	function logout()
	{
		?>
			<script type="text/javascript">            
				localStorage.clear();
			</script>
		<?php
		
		$arraydata = array(
			'id'  => '',
			'username'  => '',
			'id_profile'  => '',
			'one_name'  => '',
			'one_last_name'  => '',
			'iat'  => '',
			'exp'  => '',
			'token'  => '',
			'logged_in' => false,
		);

		$this->session->unset_userdata($arraydata);
		$this->session->sess_destroy();

		redirect('Login');
	}
}
