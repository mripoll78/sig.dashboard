<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_menu extends CI_Model{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
    public function lister_menu_admin(){
		$this->db->select('m.name as name_menu, tm.name, ut.username, asg.date_system');
		$this->db->from('menu_table m');
		$this->db->join('asig_menu_usuario_table asg','m.id = asg.id_menu');
        $this->db->join('user_table ut','asg.id_usuario = ut.id');
        $this->db->join('type_menu_table tm','m.id_type_menu = tm.id');        
		$datos = $this->db->get();
		return $datos->result();
	}
}