<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_register extends CI_Model{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function post_people($datos)
	{        
        $this->db->insert('people_table',$datos);
        return $this->db->insert_id();
	}   

	public function post_client($datos)
	{        
        $this->db->insert('client_table',$datos);
        return $this->db->insert_id();
	}   

	public function post_client_proyect($datos)
	{
        
        $this->db->insert('client_draft_table',$datos);
        return $this->db->insert_id();
	} 

	public function post_user_proyect($datos)
	{
        
        $this->db->insert('user_draft_table',$datos);
        return $this->db->insert_id();
	}

	public function post_user($datos)
	{
        
        $this->db->insert('user_table',$datos);
        return $this->db->insert_id();
	} 

	public function post_proyect($datos)
	{        
        $this->db->insert('draft_table',$datos);
        return $this->db->insert_id();
	}

	public function get_verifi_user_menu_id($id_usuario, $id_menu)
	{
		$this->db->select('*');
		$this->db->from('asig_menu_usuario_table');
		$this->db->where('id_usuario', '=', $id_usuario);
		$this->db->where('id_menu', '=', $id_menu);
		
		$datos = $this->db->get();
		return $datos->result();
	}

	public function post_asig_menu($datos)
	{        
        $this->db->insert('asig_menu_usuario_table',$datos);
        return $this->db->insert_id();
	}
	
	public function get_verifi_people_id($identification)
	{
		$this->db->select('*');
		$this->db->from('people_table');
		$this->db->where('identification',$identification);
		$this->db->where('identification', '!=', '');
		
		$datos = $this->db->get();
		return $datos->result();
	}

	public function get_verifi_user_username($username)
	{
		$this->db->select('*');
		$this->db->from('user_table');
		$this->db->where('username',$username);

		$datos = $this->db->get();
		return $datos->result();
	}

	public function get_verifi_people_email($email)
	{
		$this->db->select('*');
		$this->db->from('people_table');
		$this->db->where('email',$email);

		$datos = $this->db->get();
		return $datos->result();
	}

	public function get_verifi_client_id($identification)
	{
		$this->db->select('*');
		$this->db->from('client_table');
		$this->db->where('identification',$identification);
		
		$datos = $this->db->get();
		return $datos->result();
	}

	public function get_verifi_client_email($email)
	{
		$this->db->select('*');
		$this->db->from('client_table');
		$this->db->where('email',$email);

		$datos = $this->db->get();
		return $datos->result();
	}
}