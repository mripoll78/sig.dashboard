<?php

class M_header extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    # Query para traer menus asignados
    public function get_lister_menus_father($id){
      $this->db->select('m.name, m.id, m.binder, m.view, m.claseico as icono, m.id_menu_father, m.collapse, m.heading');
      $this->db->from('menu_table m');
      $this->db->join('asig_menu_usuario_table amu','m.id = amu.id_menu');
      $this->db->join('type_menu_table tpm','m.id_type_menu = tpm.id');
      $this->db->join('user_table u','amu.id_usuario = u.id');
      $this->db->join('profile_table r','u.id_profile = r.id');
      $this->db->where('u.id', $id);
      $this->db->where('tpm.sigla','ME_PADRE');
      $this->db->where('tpm.id',1);
      $datos = $this->db->get();
      return $datos->result();
    }

    # Query para traer los menus de nivel 2
    public function get_lister_menu_son($id){
      $this->db->select('m.name, m.id, m.binder, m.view, m.claseico as icono, m.id_menu_father, tpm.id');
      $this->db->from('menu_table m');
      $this->db->join('asig_menu_usuario_table amu','m.id = amu.id_menu');
      $this->db->join('type_menu_table tpm','m.id_type_menu = tpm.id');
      $this->db->join('user_table u','amu.id_usuario = u.id');
      $this->db->join('profile_table r','u.id_profile = r.id');
      $this->db->where('u.id', $id);
      $this->db->where('tpm.sigla','ME_HIJO');
      $this->db->where('tpm.id',2);
      $datos = $this->db->get();
      return $datos->result();
    }
}
