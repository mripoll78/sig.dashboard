<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_proyect extends CI_Model{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	    
    function get_proyect($q){
		return $this->db->get_where('user_draft_table',$q);
	}

	function lister_proyect(){
		$this->db->select('dt.id, dt.id_status, dt.name, dt.manager, dt.phone, dt.email, ct.valor as id_city, dt.address, dt.date_system');
		$this->db->from('draft_table dt');
		$this->db->join('city_table ct','dt.id_city = ct.id');
		$datos = $this->db->get();
		return $datos->result();
	}
}