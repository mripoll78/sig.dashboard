<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_login extends CI_Model{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function get_user($q) {
		return $this->db->get_where('user_table',$q);
	}
	
	function get_people($q) {
		return $this->db->get_where('people_table',$q);
	}
}