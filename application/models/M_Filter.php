<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_filter extends CI_Model{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
    }    
	
	public function list_city(){
		$this->db->select('*');
		$this->db->from('city_table');
		$datos = $this->db->get();
		return $datos->result();
	}

	public function list_type_id(){
		$this->db->select('*');
		$this->db->from('type_id_table');
		$datos = $this->db->get();
		return $datos->result();
	}

	public function list_proyect(){
		$this->db->select('*');
		$this->db->from('draft_table');
		$datos = $this->db->get();
		return $datos->result();
	}

	public function list_profile(){
		$this->db->select('*');
		$this->db->from('profile_table');
		$this->db->where('id != 1');
		$datos = $this->db->get();
		return $datos->result();
	}

	public function list_user(){
		$this->db->select('*');
		$this->db->from('user_table');
		$this->db->where('id_profile = 2');
		$datos = $this->db->get();
		return $datos->result();
	}

	public function list_menu(){
		$this->db->select('*');
		$this->db->from('menu_table');
		$datos = $this->db->get();
		return $datos->result();
	}
}