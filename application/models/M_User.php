<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_user extends CI_Model{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
    public function lister_user_client($id_draft){
		$this->db->select('*');
		$this->db->from('client_table c');
		$this->db->join('client_draft_table cd','c.id = cd.id_client');
		$this->db->where('cd.id_draft',$id_draft);
		$datos = $this->db->get();
		return $datos->result();
	}	

	public function lister_user_empleado($id_draf){
		$this->db->select('p.one_name, p.one_last_name, p.email, p.phone, p.date_system, dt.name');
		$this->db->from('user_table c');
		$this->db->join('people_table p','c.id_people = p.id');
		$this->db->join('user_draft_table udt','c.id = udt.id_user');
		$this->db->join('draft_table dt','udt.id_draft = dt.id');
		$this->db->where('c.id_profile = 5');
		$this->db->where('udt.id_draft', $id_draf);

		$datos = $this->db->get();
		return $datos->result();
	}

	public function lister_user_client_admin(){
		$this->db->select('p.one_name, p.one_last_name, p.email, p.phone, p.date_system, dt.name');
		$this->db->from('user_table c');
		$this->db->join('people_table p','c.id_people = p.id');
		$this->db->join('user_draft_table udt','c.id = udt.id_user');
		$this->db->join('draft_table dt','udt.id_draft = dt.id');
		$this->db->where('c.id_profile = 2');
		$datos = $this->db->get();
		return $datos->result();
	}
}